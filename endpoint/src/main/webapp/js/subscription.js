$(document).ready(function () {
    $("#add_new_subscription").click(function () {
        $.redirect('/endpoint/add_subscription.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $.get({
        url: 'rest/get_subs',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function (data) {
        var json = $.parseJSON(data);

        var dataSet = []
        for (var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.customerId, obj.servicePlanId, obj.usedSeats])
        }

        //$("#customer_list_id").html(data);
        $('#subscrition_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    {title: "customerId"},
                    {title: "servicePlanId"},
                    {title: "usedSeats"}
                ]
            });
    });


});