$(document).ready(function () {
    $("#add_new_plan").click(function () {
        $.redirect('/endpoint/add_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $.get({
        url: 'rest/get_plans',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function (data) {
        var json = $.parseJSON(data);

        var dataSet = []
        for (var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.name, obj.details, obj.maxSeats, obj.minSeats, obj.feePerUnit])
        }

        //$("#customer_list_id").html(data);
        $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    {title: "name"},
                    {title: "details"},
                    {title: "maxSeats"},
                    {title: "minSeats"},
                    {title: "feePerUnit"}
                ]
            });
    });


});