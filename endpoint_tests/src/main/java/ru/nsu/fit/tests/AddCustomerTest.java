package ru.nsu.fit.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.AddEntityService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.browser.LoginService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;


/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AddCustomerTest {
    private Browser browser = null;

    @BeforeMethod
    public void beforeMethod() {
        browser = BrowserService.openNewBrowser();
        LoginService.doLogin(browser);
    }


    @Test
    @Title("Add customer")
    @Description("add customer after login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void testAddCustomer() throws Exception {
        AddEntityService.addCustomer(browser);
    }
//
//    @Test
//    @Title("Add plan")
//    @Description("add plan after login")
//    @Severity(SeverityLevel.CRITICAL)
//    @Features("Customer feature")
//    public void testAddPlan() throws Exception {
//        AddEntityService.addPlan(browser);
//    }
//
//    @Test
//    @Title("Add user")
//    @Description("add user after login")
//    @Severity(SeverityLevel.CRITICAL)
//    @Features("Customer feature")
//    public void testAddUser() throws Exception {
//        AddEntityService.addUser(browser);
//    }
//
//    @Test
//    @Title("Add subscription")
//    @Description("add subscription after login")
//    @Severity(SeverityLevel.CRITICAL)
//    @Features("Customer feature")
//    public void testAddSubscription() throws Exception {
//        AddEntityService.addSub(browser);
//    }


    @AfterMethod
    public void afterMethod() {
        if (browser != null)
            browser.close();
    }


}




