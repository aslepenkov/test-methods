package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void testCreateNewCustomer() {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        //expectedEx.expectMessage("Password's length should be more or equal 6 symbols");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123", 0);
    }


    @Test
    public void testCreateNewCustomerWithShortFirstName1() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("J", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstName2() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("J", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstName() {
      expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("Johnmbulirhf", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstName13() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("Johnmbulirhff", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }


    @Test
    public void testCreateNewCustomerWithShortLastName1() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("Jadf", "W", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortLastName2() {
       expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("Jadf", "Wi", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Johnmbulirhf", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastName13() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Johnmbulirhff", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass3() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass2() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "12", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass4() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "1234", 0);
    }


    @Test
    public void testCreateNewCustomerWithLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass1() {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe", 0);
    }


    @Test
    public void testCreateNewCustomerWithLongPass2() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe12", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }

    //[AS]
    @Test
    public void testCreateNewCustomerWithNullPassword() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", null, 0);
    }

    @Test
    public void testCreateNewCustomerWithNullFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData(null, "Wick", "john_wick@gmail.com", "m3g4Pa88w0rd", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", null, "john_wick@gmail.com", "m3g4Pa88w0rd", 0);
    }

    @Test
    public void testCreateNewCustomerWithMoneyLessZero0() {

        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "m3g4Pa88w0rd", 0);
    }

    @Test
    public void testCreateNewCustomerWithMoneyLessZero() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "m3g4Pa88w0rd", -1);
    }


    @Test
    public void testCreateNewCustomerWithMoneyMinInt() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "m3g4Pa88w0rd", Integer.MIN_VALUE);
    }

    @Test
    public void testCreateNewUserIncorrectEmail() {
        expectedEx.expect(IllegalArgumentException.class);
        new Customer.CustomerData("John", "Wick", "incorrect@mail", "strongpass", 0);
        new Customer.CustomerData("John", "Wick", "incorrectmail", "strongpass", 0);
        new Customer.CustomerData("John", "Wick", "1", "strongpass", 0);
        new Customer.CustomerData("John", "Wick", "*", "strongpass", 0);
        new Customer.CustomerData("John", "Wick", "null", "strongpass", 0);

    }
}
