$(document).ready(function () {
    $("#create_plan_id").click(
        function () {
            var name = $("#name_id").val();
            var details = $("#details_id").val();
            var mxs = $("#maxSeats_id").val();
            var mns = $("#minSeats_id").val();
            var fee = $("#feePerUnit_id").val();

            // check fields
            if (false) {
                // $('input[type="text"],input[type="password"]').css("border","2px solid red");
                // $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                // alert("Email or password is empty");
            } else {
                $.post({
                    url: 'rest/create_plan',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "name": name,
                        "details": details,
                        "maxSeats": mxs,
                        "minSeats": mns,
                        "feePerUnit": fee,
                    })
                }).done(function (data) {
                    $.redirect('/endpoint/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});