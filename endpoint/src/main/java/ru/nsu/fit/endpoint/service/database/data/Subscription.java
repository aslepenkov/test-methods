package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription extends Entity<Subscription.SubscriptionData> {
    private UUID id;

    public Subscription(Subscription.SubscriptionData data, UUID id) {
        super(data);
        this.id = id;
        //todo validate
    }

    public UUID getId() {
        return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubscriptionData {

        @JsonProperty("customerId")
        private UUID customerId;
        @JsonProperty("servicePlanId")
        private UUID servicePlanId;
        @JsonProperty("usedSeats")
        private int usedSeats;

        private SubscriptionData() {
        }

        public SubscriptionData(UUID customerId, UUID servicePlanId, int usedSeats) {
            this.customerId = customerId;
            this.servicePlanId = servicePlanId;
            this.usedSeats = usedSeats;
        }

        public UUID getCustomerId() {
            return customerId;
        }

        public UUID getServicePlanId() {
            return servicePlanId;
        }

        public int getUsedSeats() {
            return usedSeats;
        }
    }
}
