package ru.nsu.fit.services.browser.screens;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class PlansScreen implements AbstractScreen {
    private Browser browser;

    public PlansScreen(Browser browser) {
        this.browser = browser;
    }

    public void open() {
        browser.openPage("http://localhost:8081/endpoint/plans.html");
    }

    public void addPlan(String planName, String planDetalis, int planMaxSeats, int planMinSeats, int planFee) {

        browser.waitForElement(By.id("add_new_plan"));
        browser.getElement(By.id("add_new_plan")).click();

        browser.getElement(By.id("name_id")).clear();
        browser.getElement(By.id("name_id")).sendKeys(planName);

        browser.getElement(By.id("details_id")).clear();
        browser.getElement(By.id("details_id")).sendKeys(planDetalis);


        browser.getElement(By.id("maxSeats_id")).clear();
        browser.getElement(By.id("maxSeats_id")).sendKeys(Integer.toString(planMaxSeats));

        browser.getElement(By.id("minSeats_id")).clear();
        browser.getElement(By.id("minSeats_id")).sendKeys(Integer.toString(planMinSeats));


        browser.getElement(By.id("feePerUnit_id")).clear();
        browser.getElement(By.id("feePerUnit_id")).sendKeys(Integer.toString(planFee));

        browser.getElement(By.id("create_plan_id")).click();

    }
}