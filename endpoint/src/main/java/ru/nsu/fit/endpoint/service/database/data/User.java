package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User extends Entity<User.UserData> {

    private UUID id;

    public User(UserData data, UUID id) {
        super(data);
        this.id = id;
        //data.validate();
    }

    public UUID getId() {
        return id;
    }

    public enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserData {

        @JsonProperty("subscriptionIds")
        private UUID[] subscriptionIds;

        @JsonProperty("customerId")
        private UUID customerId;
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */

        @JsonProperty("firstName")
        private String firstName;
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */

        @JsonProperty("lastName")
        private String lastName;
        /* указывается в виде email, проверить email на корректность */

        @JsonProperty("login")
        private String login;
        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */

        @JsonProperty("pass")
        private String pass;

        @JsonProperty("userRole")
        private UserRole userRole;

        private UserData() {
        }

        public UserData(UUID customerId, UUID[] subscriptionIds, String firstName, String lastName, String login, String pass, UserRole userRole) {

            this.customerId = customerId;
            this.subscriptionIds = subscriptionIds;
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.pass = pass;
            this.userRole = userRole;

            validate(customerId, subscriptionIds, firstName, lastName, login, pass, userRole);
        }

        public static void validate(UUID customerId, UUID[] subscriptionIds, String firstName, String lastName, String login, String pass, UserRole userRole) {
            Validate.notNull(customerId);
            Validate.notNull(subscriptionIds);
            Validate.notNull(userRole);
           // Customer.CustomerData.validate(firstName, lastName, login, pass, 1);

            Validate.notNull(firstName, "First name cannot be null!");
            Validate.notNull(login, "First name cannot be null!");
            Validate.isTrue(!firstName.matches(".*\\d+.*"), "No symbols on First Name");
            Validate.isTrue(Character.isUpperCase(firstName.charAt(0)), "First name uppercase");
            for (int i = 1; i < firstName.length(); i++) {
                Validate.isTrue(Character.isLowerCase(firstName.toCharArray()[i]), "First name lowercase");
            }

            Validate.isTrue(login.matches("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"), "incorrect e-mail");
            Validate.isTrue(firstName.length() >= 2 && firstName.length() <= 12, "Incorrect length of first name");

            try {
                Validate.notNull(lastName, "First name cannot be null!");
                Validate.isTrue(!lastName.matches(".*\\d+.*"), "No symbols on Last Name");
                Validate.isTrue(Character.isUpperCase(lastName.charAt(0)), "First name uppercase");
                for (int i = 1; i < lastName.length(); i++) {
                    Validate.isTrue(Character.isLowerCase(lastName.toCharArray()[i]), "First name lowercase");
                }
                Validate.isTrue(lastName.length() >= 2 && lastName.length() <= 12, "Incorrect length of last name");
            } catch (RuntimeException e) {
                throw new IllegalArgumentException();
            }

            try {
                Validate.notNull(pass, "Pass name cannot be null!");
                Validate.isTrue(pass.length() >= 6 && pass.length() <= 12, "Incorrect length of password");
                Validate.isTrue(!pass.toLowerCase().contains(firstName.toLowerCase()), "Password can not contain first name");
                Validate.isTrue(!pass.toLowerCase().contains(lastName.toLowerCase()), "Password can not contain last name");
                Validate.isTrue(!pass.toLowerCase().contains("qwerty"), "Password can not contain last name");
                Validate.isTrue(!pass.toLowerCase().contains("1234"), "Password can not contain last name");
                Validate.isTrue(!pass.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,12}"), "incorrect password");
            } catch (RuntimeException e) {
                throw new IllegalArgumentException();
            }
        }

        public UUID[] getSubscriptionIds() {
            return subscriptionIds;
        }

        public UUID getCustomerId() {
            return customerId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getLogin() {
            return login;
        }

        public String getPass() {
            return pass;
        }

        public String getUserRole() {
            return userRole.name().toString();
        }

    }
}
