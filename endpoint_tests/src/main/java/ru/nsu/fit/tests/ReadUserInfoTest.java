package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;


public class ReadUserInfoTest {
    @Test
    @Title("Read User Info")
    @Description("Read User info from base")
    @Severity(SeverityLevel.NORMAL)
    @Features("Customer feature")
    public void getUserInfo() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("get_user_id/user@hismail.com");

        Invocation.Builder invocationBuilder = webTarget.request();
        Response response = invocationBuilder.get();

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);
    }
}
