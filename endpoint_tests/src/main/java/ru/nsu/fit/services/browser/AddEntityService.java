package ru.nsu.fit.services.browser;

import ru.nsu.fit.services.browser.screens.CustomersScreen;
import ru.nsu.fit.services.browser.screens.PlansScreen;
import ru.nsu.fit.services.browser.screens.SubscriptionsScreen;
import ru.nsu.fit.services.browser.screens.UsersScreen;

public class AddEntityService {
    private static final String EMAIL = "admin";
    private static final String PASS = "setup";
    private static final String CUSTOMER_FIRST_NAME = "Nathan";
    private static final String CUSTOMER_LAST_NAME = "Smith";
    private static final String CUSTOMER_EMAIL = "Nathan.Smith@gmail.com";
    private static final String CUSTOMER_PASS = "Nathan.Smith@gmail.com";

    private static final String PLAN_NAME = "Amazing plan";
    private static final String PLAN_DETALIS = "There's plan detalis and description";
    private static final int PLAN_MAX_SEATS = 50;
    private static final int PLAN_MIN_SEATS = 10;
    private static final int PLAN_FEE = 5;
    private static final String USER_FIRST_NAME = "Kevin";
    private static final String USER_LAST_NAME = "Jen";
    private static final String USER_LOGIN = "Kevin.jen@nomail.com";
    private static final String USER_PASSWORD = "a!4fFadd9";
    private static final String SUBSCRIPTION_CUSTOMER = "15e5951d-c780-4ce3-b489-a515898901aa";
    private static final String SUBSCRIPTION_PLAN = "b380db1d-a389-4b5a-91e7-42b8e8eeeab7";
    private static final String SUBSCRIPTION_SEATS = "5";


    public static void addCustomer(Browser bro) {
        CustomersScreen cs = new CustomersScreen(bro);
        cs.addCustomer(CUSTOMER_FIRST_NAME, CUSTOMER_LAST_NAME, CUSTOMER_EMAIL, CUSTOMER_PASS);
    }

    public static void addUser(Browser bro) {
        UsersScreen ps = new UsersScreen(bro);
        ps.open();
        ps.addUser(USER_FIRST_NAME, USER_LAST_NAME, USER_LOGIN, USER_PASSWORD);
    }

    public static void addPlan(Browser bro) {
        PlansScreen ps = new PlansScreen(bro);
        ps.open();
        ps.addPlan(PLAN_NAME, PLAN_DETALIS, PLAN_MAX_SEATS, PLAN_MIN_SEATS, PLAN_FEE);
    }

    public static void addSub(Browser bro) {
        SubscriptionsScreen cs = new SubscriptionsScreen(bro);
        cs.open();
        cs.addSubscription(SUBSCRIPTION_CUSTOMER, SUBSCRIPTION_PLAN, SUBSCRIPTION_SEATS);
    }
}
