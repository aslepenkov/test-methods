package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CreateSubsciptionTest {
    @Test
    @Title("Create subscription")
    @Description("Create subscription on base")
    @Severity(SeverityLevel.CRITICAL)
    @Features("App feature")
    public void createSubscription() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("create_sub");


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(
                "{\n" +
                        "\t\"customerId\":\"4645ad8a-b11c-4d3b-986a-e31e11287c51\",\n" +
                        "    \"servicePlanId\":\"00e6cd57-12e1-4a89-ab39-da2c82d80147\",\n" +
                        "    \"usedSeats\":\"3\"\n" +
                        "}", MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);
    }

    @Test(dependsOnMethods = "createSubscription")
    @Title("Delete plan")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void deleteSub() {
        // ClientConfig clientConfig = new ClientConfig();
//
        // HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        // clientConfig.register(feature);
//
        // clientConfig.register(JacksonFeature.class);
//
        // Client client = ClientBuilder.newClient(clientConfig);
//
        // WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("drop_sub");
//
        // Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        // Response response = invocationBuilder.post(null);
//
        // AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
//
        // //System.out.println("response = " + response.getStatusInfo() );
        // Assert.assertEquals(response.getStatus(), 200);
    }


}


/*



 */