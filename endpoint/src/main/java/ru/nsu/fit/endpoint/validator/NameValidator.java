package ru.nsu.fit.endpoint.validator;


import org.apache.commons.validator.routines.IntegerValidator;

public class NameValidator {
    private static NameValidator ourInstance = new NameValidator();

    private NameValidator() {
    }

    public static NameValidator getInstance() {
        return ourInstance;
    }

    public boolean isValid(String name) {
        if (name == null) {
            return false;
        }
        if (!IntegerValidator.getInstance().isInRange(name.length(), 2, 12)) {
            return false;
        }
        char ch = name.charAt(0);
        if (!(Character.isLetter(ch) && Character.isUpperCase(ch))) {
            return false;
        }
        for (int i = 1; i < name.length(); i++) {
            ch = name.charAt(i);
            if (!(Character.isLetter(ch) && Character.isLowerCase(ch))) {
                return false;
            }
        }
        return true;
    }
}