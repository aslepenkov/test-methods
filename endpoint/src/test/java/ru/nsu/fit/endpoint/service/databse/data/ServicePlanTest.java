package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Plan;


public class ServicePlanTest {


    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewServicePlan() {
        new Plan.PlanData("servicePlan", "details", 99, 240, 0);
    }

    @Test
    public void testCreateNewServicePlanShortName() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "se", "details", 99, 24, 0);
    }

    @Test
    public void testCreateNewServicePlanLongName() {
        expectedEx.expect(IllegalArgumentException.class);
        String str128 = new String(new char[128]).replace('\0', 'a');
        new Plan.PlanData( str128, "details", 99, 24, 0);
    }

    @Test
    public void testCreateNewServicePlanLongDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        String str1024 = new String(new char[1024]).replace('\0', 'a');
        new Plan.PlanData( "servicePlan", str1024, 99, 24, 0);
    }

    @Test
    public void testCreateNewServicePlanShortDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "a", 99, 24, 0);
    }

    @Test
    public void testCreateNewServicePlanBigMaxSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 999999, 24, 0);
    }

    @Test
    public void testCreateNewServicePlanSmallMaxSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 1, 0, 0);
    }

    @Test
    public void testCreateNewServicePlanSmallMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 99, 1, 0);
    }

    @Test
    public void testCreateNewServicePlanBigMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 99, 999999, 0);
    }

    @Test
    public void testCreateNewServicePlanIncorrectSeatsMinMax() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 20, 20, 0);
    }

    @Test
    public void testCreateNewServicePlanNegativeFeePerUnit() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 99, 24, -1);
    }

    @Test
    public void testCreateNewServicePlanBigFeePerUnit() {
        expectedEx.expect(IllegalArgumentException.class);
        new Plan.PlanData( "servicePlan", "details", 99, 24, 1000000);
    }


}