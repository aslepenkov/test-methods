package ru.nsu.fit.endpoint.validator;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PasswordValidator {
    private static PasswordValidator ourInstance = new PasswordValidator();

    private List<String> easyPasswords = new ArrayList<>();

    private PasswordValidator() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.getClass().getClassLoader().getResourceAsStream("easy-passwords.txt"))
        )) {
            String s;
            while ((s = reader.readLine()) != null) {
                if (!s.isEmpty()) {
                    easyPasswords.add(s.toLowerCase());
                }
            }
        } catch (IOException e) {
            // NOOP
        }
    }

    public static PasswordValidator getInstance() {
        return ourInstance;
    }

    public boolean isNotEmpty(String password) {
        return password != null;
    }

    public boolean isValidInterval(String password) {
        return org.apache.commons.validator.routines.IntegerValidator.getInstance().isInRange(password.length(), 6, 12);
    }

    public boolean isNotEasy(String password) {
        return !easyPasswords.contains(password.toLowerCase());
    }

    public boolean isNotContains(String password, List<String> stringList) {
        for (String s : stringList) {
            if (password.toLowerCase().contains(s.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
