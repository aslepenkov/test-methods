package ru.nsu.fit.services.browser;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.screens.LoginScreen;

public class LoginService {
    private static final String EMAIL = "admin";
    private static final String PASS = "setup";

    public static void doLogin(Browser bro) {
        LoginScreen ls = new LoginScreen(bro);
        ls.open();

        bro.waitForElement(By.id("email"));
        bro.getElement(By.id("email")).sendKeys(EMAIL);
        bro.getElement(By.id("password")).sendKeys(PASS);
        bro.getElement(By.id("login")).click();
    }

    public static String getLogin() {
        return EMAIL;
    }

    public static String getPassword() {
        return PASS;
    }
}
