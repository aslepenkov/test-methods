package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.util.UUID;

/**
 * @author aslepenkov (a.slepenkov@g.nsu.ru)
 */
public class UserTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() {
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserNullFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, null, "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserNullLast() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", null, "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserNullPass() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", null, User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserShortFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "J", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserShortLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "S", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserLongFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "qwertyuiopasdfghjklzxcvbn", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserLongLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "qwertyuiopasdfghjklzxcvbn", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserUppercaseFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "JOhn", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserLowerFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "john", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserUppercaseLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "SmIth", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserLowerLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }


    @Test
    public void testCreateNewUserNumberInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John1", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserNumberInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith0", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserSymbolInFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "$John$", "Smith", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserSymbolInLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "S*mith##;*", "correct@mail.com", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserIncorrectEmail() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "incorrect@mail", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "incorrectmail", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "1", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "*", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "null", "m3g4Pa88w0rd", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "qwas", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "mjdafjfqge1vcaadfadfjdabkjf", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserSimplePass() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "123456789", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserSimplePass2() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "qwerty123", User.UserRole.BILLING_ADMINISTRATOR);
    }


    @Test
    public void testCreateNewUserPassContainsFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "johnlgfda", User.UserRole.BILLING_ADMINISTRATOR);
    }

    @Test
    public void testCreateNewUserPassContainsLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        new User.UserData(UUID.randomUUID(), new UUID[]{UUID.randomUUID(), UUID.randomUUID()}, "John", "Smith", "correct@mail.com", "smith!rocks", User.UserRole.BILLING_ADMINISTRATOR);
    }


}