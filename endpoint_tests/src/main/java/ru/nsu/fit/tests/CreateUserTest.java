package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CreateUserTest {
    @Test
    @Title("Create User")
    @Description("Create User on base")
    @Severity(SeverityLevel.NORMAL)
    @Features("App feature")
    public void createUser() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("create_user");


        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(
                "{\n" +
                        "\t\"subscriptionIds\":[\"00e6cd57-11e1-4a89-ab39-da2c82d80147\"],\n" +
                        "    \"customerId\":\"00e6cd57-11e1-4a89-ab39-da2c82d80147\",\n" +
                        "    \"firstName\":\"Username\",\n" +
                        "    \"lastName\":\"Username\",\n" +
                        "    \"login\":\"user@hismail.com\",\n" +
                        "    \"pass\":\"qweYtyu12\",\n" +
                        "    \"userRole\":\"USER\" \n" +
                        "}", MediaType.APPLICATION_JSON));

        String s = response.readEntity(String.class);

        AllureUtils.saveTextLog("Response: " + s);
        Assert.assertEquals(response.getStatus(), 200);

        //
        /*

        {
	"subscriptionIds":["00e6cd57-11e1-4a89-ab39-da2c82d80147"],
    "customerId":"00e6cd57-11e1-4a89-ab39-da2c82d80147",
    "firstName":"username",
    "lastName":"username",
    "login":"user@hismail.com",
    "pass":"qwertyu1234567",
    "userRole":"USER"
}
         */
        //создание пользователя в базе
    }

}
