package ru.nsu.fit.services.browser.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.nsu.fit.services.browser.Browser;

public class CustomersScreen implements AbstractScreen {
    private Browser browser;


    public CustomersScreen(Browser browser) {
        this.browser = browser;
    }

    public void open() {
        browser.openPage("http://localhost:8081/endpoint/customers.html");
    }

    public void addCustomer(String firstName, String lastName, String email, String password) {
        //browser.waitForElement(By.id("customer_list_id"));
        WebElement add_new_customer = browser.getElement(By.id("add_new_customer"));

        System.out.println(add_new_customer);
        add_new_customer.click();
        //browser.getElement(By.id("add_new_customer")).click();

        browser.waitForElement(By.id("create_customer_id"));

        browser.getElement(By.id("first_name_id")).clear();
        browser.getElement(By.id("first_name_id")).sendKeys(firstName);
        browser.getElement(By.id("last_name_id")).clear();
        browser.getElement(By.id("last_name_id")).sendKeys(lastName);
        browser.getElement(By.id("email_id")).clear();
        browser.getElement(By.id("email_id")).sendKeys(email);
        browser.getElement(By.id("password_id")).clear();
        browser.getElement(By.id("password_id")).sendKeys(password);

        browser.getElement(By.id("create_customer_id")).click();
    }
}