$(document).ready(function () {
    $("#add_new_user").click(function () {
        $.redirect('/endpoint/add_user.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $.get({
        url: 'rest/get_users',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function (data) {
        var json = $.parseJSON(data);

        var dataSet = []
        for (var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.pass])
        }

        //$("#customer_list_id").html(data);
        $('#user_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    {title: "firstName"},
                    {title: "lastName"},
                    {title: "login"},
                    {title: "pass"}
                ]
            });
    });


});