package ru.nsu.fit.services.browser.screens;

public interface AbstractScreen {
    void open();
}
