package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("")
public class RestService {
    private static final Logger logger = LoggerFactory.getLogger("REST_LOG");

    @RolesAllowed(Roles.UNKNOWN)
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity("{\"status\": \"OK\"}").build();
    }

    @RolesAllowed({Roles.UNKNOWN, Roles.ADMIN})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomers() {
        try {
            List<Customer.CustomerData> result = DBService.getCustomers();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_plans")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        try {
            List<Plan.PlanData> result = DBService.getPlans();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_users")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        logger.debug("get_users");
        try {
            List<User.UserData> result = DBService.getUsers();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_subs")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSubscriptions() {
        try {
            List<Subscription.SubscriptionData> result = DBService.getSubscriptions();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.ok().entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/create_user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(String userDataJson) {
        try {
            User.UserData customerData = JsonMapper.fromJson(userDataJson, User.UserData.class);
            DBService.createUser(customerData);
            return Response.status(200).entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/create_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPlan(String planDataJson) {
        try {
            Plan.PlanData planData = JsonMapper.fromJson(planDataJson, Plan.PlanData.class);
            DBService.createPlan(planData);
            return Response.status(200).entity(planData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/drop_plan")
    public Response deletePlan(String planDataJson) {
        try {
            DBService.deletePlan();
            return Response.status(200).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/drop_cust")
    public Response deleteCustomer(String planDataJson) {
        try {
            DBService.deleteCustomer();
            return Response.status(200).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/drop_sub")
    public Response deleteSubscription(String planDataJson) {
        try {
            DBService.deleteSub();
            return Response.status(200).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/create_sub")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createSubscription(String userDataJson) {
        try {
            Subscription.SubscriptionData subData = JsonMapper.fromJson(userDataJson, Subscription.SubscriptionData.class);
            DBService.createSubscription(subData);
            return Response.status(200).entity(subData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed("USER")
    @GET
    @Path("/get_user_id/{user_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserId(@PathParam("user_login") String userLogin) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN", "USER"})
    @GET
    @Path("/get_user_subs/{user_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserSubs(@PathParam("user_login") String userLogin) {
        try {
            UUID[] ids = DBService.getUserSub(userLogin);
            return Response.status(200).entity("{\"id\":\"" + ids.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


}