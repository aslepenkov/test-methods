package ru.nsu.fit.services.browser.screens;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class SubscriptionsScreen implements AbstractScreen {
    private Browser browser;

    public SubscriptionsScreen(Browser browser) {
        this.browser = browser;
    }

    public void open() {
        browser.openPage("http://localhost:8081/endpoint/subscriptions.html");
    }

    public void addSubscription(String customerId, String planId, String usedSeats) {
        browser.waitForElement(By.id("add_new_subscription"));
        browser.getElement(By.id("add_new_subscription")).click();

        browser.getElement(By.id("customerId_id")).clear();
        browser.getElement(By.id("customerId_id")).sendKeys(customerId);

        browser.getElement(By.id("servicePlanId_id")).clear();
        browser.getElement(By.id("servicePlanId_id")).sendKeys(planId);

        browser.getElement(By.id("usedSeats_id")).clear();
        browser.getElement(By.id("usedSeats_id")).sendKeys(usedSeats);

        browser.getElement(By.id("create_subscription_id")).click();
    }
}
