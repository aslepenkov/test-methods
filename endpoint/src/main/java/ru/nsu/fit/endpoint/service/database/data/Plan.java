package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;
import org.apache.commons.validator.routines.IntegerValidator;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan extends Entity<Plan.PlanData> {
    private UUID id;

    public Plan(Plan.PlanData data, UUID id) {
        super(data);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PlanData {
        /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
        @JsonProperty("name")
        private String name;
        /* Длина не больше 1024 символов и не меньше 1 включительно */
        @JsonProperty("details")
        private String details;
        /* Не больше 999999 и не меньше 1 включительно */
        @JsonProperty("maxSeats")
        private int maxSeats;
        /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
        @JsonProperty("minSeats")
        private int minSeats;
        /* Больше ли равно 0 но меньше либо равно 999999 */
        @JsonProperty("feePerUnit")
        private int feePerUnit;

        private PlanData() {
        }

        public PlanData(String name, String details, int minSeats, int maxSeats, int feePerUnit) {
            this.name = name;
            this.details = details;
            this.maxSeats = maxSeats;
            this.minSeats = minSeats;
            this.feePerUnit = feePerUnit;
            validate(name, details, maxSeats, minSeats, feePerUnit);
        }

        public static void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit) {

            Validate.notNull(name);
            Validate.notNull(details);
            Validate.isTrue(name.length() > 2 && name.length() < 128, "name's length should be more or equal 2 symbols and less  128 symbols");
            Validate.isTrue(details.length() > 1 && details.length() < 1024, "details's length should be more or equal 1 symbols and less  1024 symbols");
            Validate.isTrue(feePerUnit >= 0, "feePerUnit must be positive value or equal 0");
            Validate.isTrue(feePerUnit <= 999999, "feePerUnit must be positive value less than 999999");
            Validate.isTrue(maxSeats >= 1, "maxSeats must be more than 1");
            Validate.isTrue(minSeats >= 1, "minSeats must be more than 1");
            Validate.isTrue(maxSeats < 999999, "maxSeats must be less than 999999");
            Validate.isTrue(minSeats < 999999, "minSeats must be less than 999999");
            Validate.isTrue(minSeats < maxSeats, "minSeats must be less than maxSeats");


            Validate.notNull(name);
            Validate.isTrue(IntegerValidator.getInstance().isInRange(name.length(), 2, 128),
                    "Name's length must be from 1 to 128"
            );

            Validate.notNull(details);
            Validate.isTrue(IntegerValidator.getInstance().isInRange(details.length(), 1, 1024),
                    "Details's length must be from 1 to 1024"
            );
            Validate.isTrue(IntegerValidator.getInstance().isInRange(maxSeats, 0, 999999),
                    "Max seats must be from 1 to 999999"
            );
            Validate.isTrue(IntegerValidator.getInstance().isInRange(minSeats, 0, 999999),
                    "Min seats must be from 1 to 999999"
            );
            Validate.isTrue(IntegerValidator.getInstance().maxValue(minSeats, maxSeats),
                    "Min seats must be greater or equal than max seats"
            );
            Validate.isTrue(IntegerValidator.getInstance().isInRange(feePerUnit, 0, 999999),
                    "Fee ber unit must be from 0 to 999999"
            );
        }


        @Override
        public String toString() {
            return "PlanData{" +
                    "name='" + name + '\'' +
                    ", details='" + details + '\'' +
                    ", maxSeats=" + maxSeats +
                    ", minSeats=" + minSeats +
                    ", feePerUnit=" + feePerUnit +
                    '}';
        }

        public String getName() {
            return name;
        }

        public String getDetails() {
            return details;
        }

        public int getMaxSeats() {
            return maxSeats;
        }

        public int getMinSeats() {
            return minSeats;
        }

        public int getFeePerUnit() {
            return feePerUnit;
        }

    }
}
