package ru.nsu.fit.services.browser.screens;

import ru.nsu.fit.services.browser.Browser;

public class LoginScreen implements AbstractScreen {
    private Browser browser;

    public LoginScreen(Browser browser) {
        this.browser = browser;
    }

    public void open() {
        browser.openPage("http://localhost:8081/endpoint");
    }
}
