package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.sql.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    //  private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    //  private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_USERS = "SELECT * FROM USER";
    private static final String SELECT_SUBS = "SELECT * FROM SUBSCRIPTION";

    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_USER = "INSERT INTO USER(id, customer_id, first_name, last_name, login, pass, user_role) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, min_seats, max_seats, fee_per_seat) values ('%s', '%s', '%s', %s, %s, %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, plan_id, customer_id, used_seats) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USER = "SELECT id FROM USER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_PASS = "SELECT pass FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USER_SUB = "SELECT pass FROM CUSTOMER WHERE login='%s'";

    private static final String DROP_PLAN = "TRUNCATE `plan`;";
    private static final String DROP_SUB = "TRUNCATE `subscription`;";
    private static final String DROP_CUST = "TRUNCATE `customer`;";
    private static final String DROP_USER = "TRUNCATE `user`;";


    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");



    private static final Object generalMutex = new Object();
    private static Connection connection;
    private static List<Customer.CustomerData> plans;

    static {
        init();
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<Customer.CustomerData> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer.CustomerData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new Customer.CustomerData(
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getInt(6)));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

//    public static UUID getCustomerIdByLogin(String customerLogin) {
//        synchronized (generalMutex) {
//            logger.info("Try to create customer");
//
//            try {
//                Statement statement = connection.createStatement();
//                ResultSet rs = statement.executeQuery(
//                        String.format(
//                                SELECT_CUSTOMER,
//                                customerLogin));
//                if (rs.next()) {
//                    return UUID.fromString(rs.getString(1));
//                } else {
//                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
//                }
//            } catch (SQLException ex) {
//                logger.debug(ex.getMessage(), ex);
//                throw new RuntimeException(ex);
//            }
//        }
//    }


    public static void createUser(User.UserData userData) {
        synchronized (generalMutex) {
            logger.info("Try to create user");

            User user = new User(userData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                //System.out.println(user.getData().getUserRole());
                String sql = String.format(INSERT_USER,
                        user.getId(),
                        user.getData().getCustomerId(),
                        user.getData().getFirstName(),
                        user.getData().getLastName(),
                        user.getData().getLogin(),
                        user.getData().getPass(),
                        user.getData().getUserRole()
                );
                statement.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void createSubscription(Subscription.SubscriptionData subscriptionData) {
        synchronized (generalMutex) {
            logger.info("Try to create subs");

            Subscription subscription = new Subscription(subscriptionData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(INSERT_SUBSCRIPTION,
                                subscription.getId(),
                                subscription.getData().getCustomerId(),
                                subscription.getData().getServicePlanId(),
                                subscription.getData().getUsedSeats()
                        ));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void createPlan(Plan.PlanData planData) {
        synchronized (generalMutex) {
            logger.info("Try to create plan");

            Plan plan = new Plan(planData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(INSERT_PLAN,
                                plan.getId(),
                                plan.getData().getName(),
                                plan.getData().getDetails(),
                                plan.getData().getMinSeats(),
                                plan.getData().getMaxSeats(),
                                plan.getData().getFeePerUnit()
                        ));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void deletePlan() {
        synchronized (generalMutex) {
            logger.info("Try to deletePlan");
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DROP_PLAN);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void deleteSub() {
        synchronized (generalMutex) {
            logger.info("Try to deleteSub");
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DROP_SUB);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void deleteCustomer() {
        synchronized (generalMutex) {
            logger.info("Try to deleteCustomer ");
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DROP_CUST);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getUserIdByLogin(String userLogin) {
        synchronized (generalMutex) {

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER,
                                userLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static String getCustomerHashByLogin(String customerLogin) {
        synchronized (generalMutex) {


            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_PASS,
                                customerLogin));
                if (rs.next()) {
                    return (rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }

    public static UUID[] getUserSub(String userLogin) {

        synchronized (generalMutex) {

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_SUB,
                                userLogin));
                if (rs.next()) {
                    return new UUID[]{UUID.fromString(rs.getString(1))};
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

    }

    public static List<Plan.PlanData> getPlans() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                List<Plan.PlanData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new Plan.PlanData(
                            rs.getString(2),
                            rs.getString(3),
                            rs.getInt(4),
                            rs.getInt(5),
                            rs.getInt(6)));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<Subscription.SubscriptionData> getSubscriptions() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_SUBS);
                List<Subscription.SubscriptionData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new Subscription.SubscriptionData(
                            UUID.fromString(rs.getString(2)),
                            UUID.fromString(rs.getString(3)),
                            rs.getInt(4)

                    ));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<User.UserData> getUsers() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_USERS);
                List<User.UserData> result = Lists.newArrayList();

                while (rs.next()) {
                  result.add(new User.UserData(
                            UUID.fromString(rs.getString(1)),
                            new UUID[]{},
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            User.UserRole.valueOf(rs.getString(7))
                    ));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
}
