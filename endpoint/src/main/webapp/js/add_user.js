$(document).ready(function () {
    $("#create_user_id").click(
        function () {
            var fName = $("#firstName_id").val();
            var lName = $("#lastName_id").val();
            var login = $("#login_id").val();
            var password = $("#pass_id").val();
            var role = $("#role_id option:selected").text();



            // check fields
            if (login == '' || password == '') {
                $('input[type="text"],input[type="password"]').css("border", "2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
                alert("Email or password is empty");
            } else {
                $.post({
                    url: 'rest/create_user',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "subscriptionIds":"[\"00e6cd57-11e1-4a89-ab39-da2c82d80147\"]",
                        "customerId":"00e6cd57-11e1-4a89-ab39-da2c82d80147",
                        "firstName": fName,
                        "lastName": lName,
                        "login": login,
                        "pass": password,
                        "userRole": role,
                    })
                }).done(function (data) {
                    $.redirect('/endpoint/users.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});