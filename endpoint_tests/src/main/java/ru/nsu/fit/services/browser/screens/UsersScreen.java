package ru.nsu.fit.services.browser.screens;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class UsersScreen implements AbstractScreen {
    private Browser browser;

    public UsersScreen(Browser browser) {
        this.browser = browser;
    }

    public void open() {
        browser.openPage("http://localhost:8081/endpoint/users.html");
    }

    public void addUser(String firstName_id, String lastName_id, String login_id, String pass_id) {
        browser.waitForElement(By.id("add_new_user"));
        browser.getElement(By.id("add_new_user")).click();

        browser.getElement(By.id("firstName_id")).clear();
        browser.getElement(By.id("firstName_id")).sendKeys(firstName_id);

        browser.getElement(By.id("lastName_id")).clear();
        browser.getElement(By.id("lastName_id")).sendKeys(lastName_id);

        browser.getElement(By.id("login_id")).clear();
        ;
        browser.getElement(By.id("login_id")).sendKeys(login_id);

        browser.getElement(By.id("pass_id")).clear();
        browser.getElement(By.id("pass_id")).sendKeys(pass_id);

        browser.getElement(By.id("create_user_id")).click();
    }
}
