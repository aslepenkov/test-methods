package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class CreatePlanTest {
    @Test
    @Title("Create plan")
    @Description("Create subscription plan on base")
    @Severity(SeverityLevel.NORMAL)
    @Features("App feature")
    public void createPlan() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("create_plan");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(
                "{\n" +
                        "\t\"name\":\"plan" + (int) (Math.random() * 100) + "\",\n" +
                        "    \"details\":\"theres no enough symbols to determine that amazing plan yea\",\n" +
                        "    \"maxSeats\":\"3\",\n" +
                        "    \"minSeats\":\"1\",\n" +
                        "    \"feePerUnit\":\"2\"\n" +
                        "}", MediaType.APPLICATION_JSON));

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);
    }


    @Test(dependsOnMethods = "createPlan")
    @Title("Delete plan")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void DeletePlan() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("drop_plan");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(null);

        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);
    }
}
